<?php
 
require_once( 'src/Facebook/FacebookSession.php' );
require_once( 'src/Facebook/FacebookRedirectLoginHelper.php' );
require_once( 'src/Facebook/FacebookRequest.php' );
require_once( 'src/Facebook/FacebookResponse.php' );
require_once( 'src/Facebook/FacebookSDKException.php' );
require_once( 'src/Facebook/FacebookRequestException.php' );
require_once( 'src/Facebook/FacebookAuthorizationException.php' );
require_once( 'src/Facebook/GraphObject.php' );
 
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
 
// start session
session_start();

// init app with app id and secret
FacebookSession::setDefaultApplication( '597131957050480','228d86ea64c27ec33d2849f1046b2a98' );
 
// login helper with redirect_uri
$helper = new FacebookRedirectLoginHelper( 'http://web-service.seryogin-ubnt.php.nixsolutions.com:8080/www/' );
 
    try {
      $session = $helper->getSessionFromRedirect();
    } catch( FacebookRequestException $ex ) {
      // When Facebook returns an error
    } catch( Exception $ex ) {
      // When validation fails or other local issues
    }

    // see if we have a session
    if ( isset( $session ) ) {
      // graph api request for user data
      $request = new FacebookRequest( $session, 'GET', '/me' );
      $response = $request->execute();
      // get response
      $graphObject = $response->getGraphObject();

      // print data
      echo '<pre>' . print_r( $graphObject, 1 ) . '</pre>';
    } else {
      // show login url
      echo '<a href="' . $helper->getLoginUrl() . '">Login</a>';
    }
