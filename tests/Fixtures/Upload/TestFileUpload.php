<?php
/**
 * Bluz Framework Component
 *
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/framework
 */

/**
 * @namespace
 */
namespace Application\Tests\Fixtures\Upload;

use Bluz\Http\FileUpload;

/**
 * TestFileUpload
 *
 * @package  Application\Tests
 *
 * @author   Taras Seryogin
 * @created  02.06.14 13:20
 */
class TestFileUpload extends FileUpload
{
    /**
     * Class that will be used to work with a downloadable file
     * @var string
     */
    protected $className = 'Application\Tests\Fixtures\Upload\TestFile';

}