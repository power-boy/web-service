<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;

/**
 * @package Application\Tests\Api
 * @author   Taras Seryogin
 */
class SigninTest extends ControllerTestCase
{
    /**
     * Test user with wrong password
     */
    public function testSigninWithWrongPassword()
    {
        $response = $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '1223'],
            'POST'
        );

        $this->assertEquals($response->getCode(), 400);

    }

    /**
     * Test user with correct password
     */
    public function testSigninWithCorrectPassword()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );

        $this->assertOk();;
    }

    public function testUserNotFound()
    {
        $response = $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test123', 'password' => '123'],
            'POST'
        );
        $this->assertEquals($response->getCode(), 404);
    }

}
