<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;
use Application\Tests\BootstrapTest;

/**
 * @package Application\Tests\Api
 * @author   Taras Seryogin
 */
class CommentTest extends ControllerTestCase
{

    public static function tearDownAfterClass()
    {
        BootstrapTest::getInstance()->getDb()->delete('comments')->where('userId = ?', 58)->execute();
    }

    public function testAddComment()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $this->dispatchUri(
            'comments/add',
            ['photoId' => 152, 'comment' => 'тестовый коммент 123'],
            'POST'
        );

        $this->assertOk();
    }

    public function testBrowseComment()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'comments/get',
            ['photoId' => 152],
            'GET'
        );
        $this->assertEquals(current($response->getBody()->getData())['comment'], 'тестовый коммент 123');
        $this->assertOk();
    }
}