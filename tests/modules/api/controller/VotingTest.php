<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;


/**
 * @package Application\Tests\Api
 * @author   Taras Seryogin
 */
class VotingTest extends ControllerTestCase
{

    public function testVotingUp()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'voting/up',
            ['photoId' => 37],
            'GET'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'You have voted');
        $this->assertEquals($response->getCode(), 201);
    }

    public function testVotingDown()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'voting/down',
            ['photoId' => 37],
            'GET'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'You have voted');
        $this->assertEquals($response->getCode(), 200);
    }

}