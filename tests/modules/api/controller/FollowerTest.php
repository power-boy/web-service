<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;


/**
 * @package Application\Tests\Api
 * @author   Taras Seryogin
 */
class FollowerTest extends ControllerTestCase
{

    public function testIfollowList()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'follower/i-follow-list'
        );

        $this->assertEquals(current($response->getBody()->getData())['login'], 'petrov');
        $this->assertOk();
    }

    public function testFollowMeList()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'follower/follows-me-list'
        );

        $this->assertEquals(current($response->getBody()->getData())['login'], 'admin');
        $this->assertOk();
    }

    public function testAddFollower()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'follower/add',
            ['userId' => 59],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'You have subscribed to this user');
        $this->assertEquals($response->getCode(), 201);
    }

    public function testDeleteFollower()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'follower/delete',
            ['userId' => 59],
            'DELETE'
        );
        $this->assertEquals(current($response->getBody()->getData()), 'You have unsubscribed from the user');
        $this->assertEquals($response->getCode(), 204);
    }
}