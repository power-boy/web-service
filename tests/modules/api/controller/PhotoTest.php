<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;
use Application\Tests\BootstrapTest;

/**
 * @package Application\Tests\Api
 * @author   Taras Seryogin
 */
class PhotoTest extends ControllerTestCase
{

    public static function setUpBeforeClass()
    {
        BootstrapTest::getInstance()->getDb()->insert('media')->setArray(
            [
                'userId' => 58,
                'module' => 'users',
                'title' => 'test',
                'type' => 'image/jpeg',
                'file' => 'uploads/58/media/test-1212.jpg',
                'preview' => 'uploads/58/media/.thumb/120x120/test-1212.jpg',
            ]
        )->execute();

    }


    public static function tearDownAfterClass()
    {
        BootstrapTest::getInstance()->getDb()->delete('media')->where('userId = ?', 58)->execute();
    }

    public function testGetMyList()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'photo/get-my-list'
        );

        $this->assertEquals(current($response->getBody()->getData())['userId'], 58);
        $this->assertOk();
    }


    public function testGetMyFriendsList()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'photo/get-my-friends-list'
        );

        $this->assertEquals(current($response->getBody()->getData())['userId'], 60);
        $this->assertOk();

    }

    public function testGetCommonList()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $this->dispatchUri(
            'photo/common-list'
        );

        $this->assertOk();
    }

    public function testUploadFile()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );

        $_FILES = array(
            'file' => array(
                'name' => 'test.jpg',
                'size' => '595284',
                'type' => 'image/jpeg',
                'tmp_name' => '/home/dev/web-service/tests/Fixtures/Upload/test.jpg',
                'error' => 0
            )
        );

        $className = 'Application\Tests\Fixtures\Upload\TestFileUpload';
        $request =  app()->getRequest();
        $request->setUploadClassName($className);

        $response = $this->dispatchUri(
                'photo/crud',
                ['title' => 'test', 'file' => $_FILES['file']],
                'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'File successfully loaded');
        $this->assertOk();
    }
}