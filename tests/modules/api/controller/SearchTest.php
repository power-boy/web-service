<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;


/**
 * @package Application\Tests\Api
 * @author   Taras Seryogin
 */
class SearchTest extends ControllerTestCase
{

    public function testSearch()
    {
        $this->dispatchUri(
            'authentication/signin',
            ['username' => 'test', 'password' => '123'],
            'POST'
        );
        $response = $this->dispatchUri(
            'search/index',
            ['username' => 'test'],
            'GET'
        );

        $this->assertEquals(current($response->getBody()->getData())['login'], 'test');
        $this->assertOk();
    }
}