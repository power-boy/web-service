<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Tests\Api;

use Application\Tests\ControllerTestCase;
use Application\UsersSignUp\Crud;
use Application\Tests\BootstrapTest;

class SignupTest extends ControllerTestCase
{

    /**
     * Setup TestCase
     */
    protected function setUp()
    {
        $this->getApp();
        Crud::getInstance()->clearErrors();

        $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Taras',
                'name' => 'Taras Seryogin',
                'email' => 'taras.seryogin@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );
    }

    /**
     * Tear Down
     *
     * @return void
     */
    protected function tearDown()
    {
        BootstrapTest::getInstance()->getDb()->delete('users')->where('login = ?', 'Taras')->execute();
    }


    public function testUserAlreadyExist()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Taras',
                'name' => 'Taras Seryogin',
                'email' => 'test@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'User with login "Taras" already exists');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testEmailAlreadyExist()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Taras1',
                'name' => 'Test User',
                'email' => 'taras.seryogin@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'User with email "taras.seryogin@gmail.com" already exists');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testPasswordNotEquals()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Tarasaa',
                'name' => 'Taras Seryogin',
                'email' => 'taras.seryoginaaaasasas@gmail.com',
                'password' => '111',
                'password2' => '11122',
            ],
            'POST'
        );
        $this->assertEquals(current($response->getBody()->getData()), 'Password is not equal');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testPasswordIsEmpty()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Tarasaaa',
                'name' => 'Taras Seryogin',
                'email' => 'taras.seryogiqqqn@gmail.com',
                'password' => '',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'Password can\'t be empty');
        $this->assertEquals($response->getCode(), 400);
    }


    public function testUriIsEmpty()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => '',
                'login' => 'Taras11',
                'name' => 'Taras Seryogin',
                'email' => 'taras.seryogin1@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'Uri can\'t be empty');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testNameIsEmpty()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Taraskkk',
                'name' => '',
                'email' => 'taras.seryogin11@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'Name can\'t be empty');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testEmailIsEmpty()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Tarasqqq',
                'name' => 'Taras Seryogin',
                'email' => '',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );
        $this->assertEquals(current($response->getBody()->getData()), 'Email can\'t be empty');
        $this->assertEquals($response->getCode(), 400);
    }


    public function testLoginIsEmpty()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => '',
                'name' => 'Taras Seryogin',
                'email' => 'taras.seryogin@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'Login can\'t be empty');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testLoginIsBigger()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Tarasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
                sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
                sssssssssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                'name' => 'Taras Seryogin',
                'email' => 'taras.seryogin@gmail.com',
                'password' => '111',
                'password2' => '111',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'Login can\'t be bigger than 255 symbols');
        $this->assertEquals($response->getCode(), 400);
    }

    public function testEmailIsBigger()
    {
        $response = $this->dispatchUri(
            'authentication/signup',
            [
                'uri' => 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
                'login' => 'Taras11',
                'name' => 'Taras Seryogin',
                'email' => 'Tarasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
                sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
                sssssssssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com',
                'password' => '111',
                'password2' => '11122',
            ],
            'POST'
        );

        $this->assertEquals(current($response->getBody()->getData()), 'Email can\'t be bigger than 255 symbols');
        $this->assertEquals($response->getCode(), 400);
    }

}
