<?php
/**
 * Get user by user name
 *
 * @author   Taras Seryogin
 * @created  20.07.11 18:39
 */
namespace Application;

use Bluz;
use Application\Auth;
use Swagger\Annotations as SWG;


return

    /**
     * @package
     * @category
     *
     * @SWG\Resource(
     *   apiVersion="0.0.1",
     *   basePath="/",
     *   resourcePath="/search",
     *   @SWG\Produces("application/json")
     * )
     * @SWG\Api(path="/search",
     *   @SWG\Operation(
     *     method="GET",
     *     summary="Get user by user name",
     *     notes="",
     *     type="User",
     *     nickname="getUserByName",
     *     @SWG\Parameter(
     *       name="username",
     *       description="The name by which we want to find the user",
     *       required=true,
     *       type="string",
     *       paramType="query"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden"),
     *     @SWG\ResponseMessage(code=404, message="Not found")
     *   )
     * )
     */

    /**
     * @method GET
     * @route /search
     * @param string $username
     * @return \closure
     */
function ($username){
    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $followerId = $this->getAuth()->getIdentity()->id;
    if (empty($followerId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        if (!empty($username)) {
            $result = Users\Table::getInstance()->search($username);
            if (!empty($result)) {
                return $result;
            } else {
                $this->getResponse()->setCode(404);
                return ['Photo not found'];
            }
        } else {
            $this->getResponse()->setCode(400);
            return ['Login is empty'];
        }
    }
};
