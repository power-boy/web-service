<?php
/**
 * User registration
 *
 * @category Application
 *
 * @author   Anton Shevchuk
 * @created  09.11.12 13:19
 */
namespace Application;

use Application\UsersFb;
use Bluz\Controller;

use Application\Auth\Table;
use Application\Facebook;
use Application\Users\Table as Table1;
use Bluz;

return
    /**
     * @return \closure
     */
    function () use ($view) {
        /**
         * @var \Application\Bootstrap $this
         * @var \Bluz\View\View $view
         */
        // change layout
        $this->useLayout('small.phtml');


        $options = $this->getConfigData('auth', 'facebook');
    if (!$options || !isset($options['appId']) || empty($options['secret'])
        || !isset($options['appId']) || empty($options['secret'])) {
        throw new Exception('Facebook authorization is not configured');
    }

    // redirect signin user to index page and init session
    if ($this->getAuth()->getIdentity()) {
        $this->redirectTo('index', 'index');
    }

    $facebook = new Facebook\Facebook(
        array(
            'appId'  => $options['appId'],
            'secret' => $options['secret'],
        )
    );
    /**
     * Should be return id of user, if he allows application.
     * In false returned 0.
     */
    $facebook->destroySession();
    $user = $facebook->getUser();


    if ($user) {
        // Proceed knowing you have a logged in user who's authenticated.
        $user_profile = $facebook->api('/me');

        /**
         * @var Auth\Table $authTable
         */
        $authTable = Auth\Table::getInstance();
        $row = $authTable->getAuthRow(Table::PROVIDER_FACEBOOK, $user_profile['id']);

        if ($row) {
            // if user has been registered
            $user = Users\Table::findRow($row->userId);

            if ($user->status != Table1::STATUS_ACTIVE) {
                $this->getMessages()->addError('User is not active');
                $this->redirectTo('index', 'index');
            }

            $user->login();
            $this->redirectTo('index', 'index');
        } else {
            // signup user
            if (!$user = $this->getAuth()->getIdentity()) {
                 $view->login = $user_profile['username'];
                 $view->email = $user_profile['email'];
                 $view->name  = $user_profile['first_name'] .' '. $user_profile['last_name'];
                 $view->foreignKey = $user_profile['id'];
            }
        }
    } else {
        /**
         * If user doesn't allow application yet, redirect him to fb page for this.
         * After this operation we will returned to this file.
         * Is user declined app, we get param 'error' => 'access_denied'
         */

        // if user declined
        if ('access_denied' == $this->getRequest()->getParam('error', null)) {
            $this->redirectTo('users', 'signin');
        }

        $login_url = $facebook->getLoginUrl(array('scope' => 'email'));
        $this->redirect($login_url);
    }

    };
