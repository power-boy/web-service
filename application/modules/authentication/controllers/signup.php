<?php
/**
 * Registration
 *
 * @author   Taras Seryogin
 * @created  20.07.11 18:39
 */
namespace Application;


use Application\UsersSignUp\Crud;
use Swagger\Annotations as SWG;
use Bluz\Controller;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/authentication",
     *      @SWG\Api(path="/authentication/signup",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="registration",
     *              notes="",
     *              type="string",
     *              nickname="userRegister",
     *                   @SWG\Parameter(
     *                   name="uri",
     *                   description="Uri for activation user",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *              @SWG\Parameter(
     *                   name="login",
     *                   description="The user name for login",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *              @SWG\Parameter(
     *                   name="name",
     *                   description="The user name ",
     *                   type="string",
     *                   paramType="form"
     *     ),
     *             @SWG\Parameter(
     *                   name="email",
     *                   description="email",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *            @SWG\Parameter(
     *                   name="password",
     *                   description="The password for login in clear text",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *                  @SWG\Parameter(
     *                   name="password2",
     *                   description="The password for confirm",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request")
     *   )
     * )
     * )
     */

    /**
     * @method POST
     * @return \closure
     */
    function () {
        /**
         * @var \Application\Bootstrap $this
         */
        $this->useJson();
        $crudController = new Controller\Crud();
        $crudController->setCrud(UsersSignUp\Crud::getInstance());
        $errorLogin = sprintf(Crud::USER_ALREADY_EXIST, $crudController->getData('login'));
        $errorEmail = sprintf(Crud::EMAIL_ALREADY_EXIST, $crudController->getData('email'));

        $result = $crudController();
        if (isset($result['errors'])) {
            $this->getResponse()->setCode(400);

            //login
            if (isset($result['errors']['login'])) {
                foreach ($result['errors']['login'] as $val) {
                    if ($val == Crud::LOGIN_EMPTY) {
                        return [Crud::LOGIN_EMPTY];
                    }
                    if ($val == Crud::LOGIN_IS_BIGGER) {
                        return [Crud::LOGIN_IS_BIGGER];
                    }
                    if (html_entity_decode($val, ENT_HTML5) == $errorLogin) {
                        return [$errorLogin];
                    }
                }
            }


            //email
            if (isset($result['errors']['email'])) {
                foreach ($result['errors']['email'] as $val) {
                    if ($val == Crud::EMAIL_EMPTY) {
                        return [Crud::EMAIL_EMPTY];
                    }
                    if ($val == Crud::EMAIL_HAS_INVALID_DOMAINE) {
                        return [Crud::EMAIL_HAS_INVALID_DOMAINE];
                    }
                    if ($val == Crud::EMAIL_IS_INVALID) {
                        return [Crud::EMAIL_IS_INVALID];
                    }
                    if ($val == Crud::EMAIL_IS_BIGGER) {
                        return [Crud::EMAIL_IS_BIGGER];
                    }
                    if (html_entity_decode($val, ENT_HTML5) == $errorEmail) {
                        return [$errorEmail];
                    }
                }
            }

            //name
            if (isset($result['errors']['name'])) {
                foreach ($result['errors']['name'] as $val) {
                    if ($val == Crud::NAME_EMPTY) {
                        return [Crud::NAME_EMPTY];
                    }
                    if ($val == Crud::NAME_IS_BIGGER) {
                        return [Crud::NAME_IS_BIGGER];
                    }
                }
            }


            //uri
            if (isset($result['errors']['uri'])) {
                foreach ($result['errors']['uri'] as $val) {
                    if ($val == Crud::URI_EMPTY) {
                        return [Crud::URI_EMPTY];
                    }
                }
            }


            //password
            if (isset($result['errors']['password'])) {
                foreach ($result['errors']['password'] as $val) {
                    if ($val == Crud::PASSWORD_EMPTY) {
                        return [Crud::PASSWORD_EMPTY];
                    }
                }
            }


            //password2
            if (isset($result['errors']['password2'])) {
                foreach ($result['errors']['password2'] as $val) {
                    if ($val == Crud::PASSWORD_NOT_EQUALS) {
                        return [Crud::PASSWORD_NOT_EQUALS];
                    }
                }
            }
        }
        return $result['row'];
    };
