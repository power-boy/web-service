<?php
/**
 * Authorization user into the system
 *
 * @author   Taras Seryogin
 * @created  20.07.11 18:39
 */
namespace Application;

use Bluz;
use Application\AuthApi;
use Application\AuthApi\Table;
use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/authentication",
     *      @SWG\Api(path="/authentication/signin",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="Authorization user into the system",
     *              notes="",
     *              type="string",
     *              nickname="loginUser",
     *                @SWG\Parameter(
     *                 name="username",
     *                 description="The user name for login",
     *                 type="string",
     *                 paramType="form"
     *     ),
     *               @SWG\Parameter(
     *                 name="password",
     *                 description="The password for login in clear text",
     *                 type="string",
     *                 paramType="form"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request")
     *   )
     * )
     * )
     */

    /**
     * @method POST
     * @return \closure
     */
function ($username, $password) {

    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $auth = AuthApi\Table::getInstance()->authenticateEquals($username, $password);
    if ($auth == Table::NOT_FOUND) {
        $this->getResponse()->setCode(404);
        return [Table::NOT_FOUND];
    }
    if ($auth == Table::WRONG_PASSWORD) {
        $this->getResponse()->setCode(400);
        return [Table::WRONG_PASSWORD];
    }
    if ($auth == Table::PENDING) {
        $this->getResponse()->setCode(403);
        return [Table::PENDING];
    }
    return [$this->getAuth()->getIdentity()];

};
