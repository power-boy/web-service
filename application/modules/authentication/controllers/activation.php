<?php
/**
 * User Activation
 *
 * @category Application
 *
 * @author   Anton Shevchuk
 * @created  05.12.12 15:17
 */
namespace Application;

use Application\Roles;
use Application\Roles\Table;
use Application\Users;
use Application\UsersActions;
use Application\UsersRoles;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/authentication",
     *      @SWG\Api(path="/authentication/activation",
     *          @SWG\Operation(
     *              method="GET",
     *              summary="Activation user into the system",
     *              notes="",
     *              type="string",
     *              nickname="loginUser",
     *                @SWG\Parameter(
     *                 name="id",
     *                 description="User id",
     *                 type="integer",
     *                 paramType="query"
     *     ),
     *               @SWG\Parameter(
     *                 name="code",
     *                 description="Activation code",
     *                 type="string",
     *                 paramType="query"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request")
     *   )
     * )
     * )
     */

    /**
     * @method GET
     * @param int $id User UID
     * @param string $code
     * @return \closure
     */
    function ($id, $code) {
        /**
         * @var \Application\Bootstrap $this
         * @var \Bluz\View\View $view
         */
        $this->useJson();
        $actionRow = UsersActions\Table::findRow(['userId' => $id, 'code' => $code]);

        if (!$actionRow) {
            $this->getResponse()->setCode(400);
            return ['Invalid activation code'];
        }

        $datetime1 = new \DateTime(); // now
        $datetime2 = new \DateTime($actionRow->expired);
        $interval = $datetime1->diff($datetime2);

        if ($actionRow->action !== UsersActions\Table::ACTION_ACTIVATION) {
            $this->getResponse()->setCode(400);
            return ['Invalid activation code'];

        } elseif ($interval->invert) {
            $this->getResponse()->setCode(400);
            $actionRow->delete();
            return ['The activation code has expired'];

        } else {
            // change user status
            $userRow = Users\Table::findRow($id);
            $userRow -> status = Users\Table::STATUS_ACTIVE;
            $userRow -> save();

            // create user role
            // get member role
            $roleRow = Roles\Table::findRowWhere(['name' => Table::BASIC_MEMBER]);
            // create relation user to role
            $usersRoleRow = new UsersRoles\Row();
            $usersRoleRow->roleId = $roleRow->id;
            $usersRoleRow->userId = $userRow->id;
            $usersRoleRow->save();

            // remove old code
            $actionRow->delete();
        }
        return false;
    };
