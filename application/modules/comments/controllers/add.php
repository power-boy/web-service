<?php
/**
 * Add comment to the photo
 *
 * @author   Taras Seryogin
 * @created  20.07.11 18:39
 */
namespace Application;

use Bluz;
use Application\Auth;
use Swagger\Annotations as SWG;

return

    /**
     * @package
     * @category
     *
     * @SWG\Resource(
     *   apiVersion="0.0.1",
     *   basePath="/",
     *   resourcePath="/comments",
     *   @SWG\Produces("application/json")
     * )
     * @SWG\Api(path="/comments",
     *   @SWG\Operation(
     *     method="POST",
     *     summary="Add comment to the photo",
     *     notes="",
     *     type="User",
     *     nickname="comment",
     *     @SWG\Parameter(
     *       name="photoId",
     *       description="id photo",
     *       required=true,
     *       type="string",
     *       paramType="form"
     *     ),
     *     @SWG\Parameter(
     *       name="comment",
     *       description="Add comment to the selected photo",
     *       required=true,
     *       type="string",
     *       paramType="form"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden")
     *   )
     * )
     */

    /**
     * @method POST
     * @route /comments
     * @param string $comment
     * @param int $photoId
     * @return \closure
     */
function ($photoId, $comment) {
    /**
     *  @var \Application\Bootstrap $this
     */
    $this->useJson();
    $userId = $this->getAuth()->getIdentity()->id;
    if (empty($userId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        if (!empty($photoId) && !empty($comment)) {
            $commentId = Comments\Table::getInstance()->addComments($userId, $photoId, $comment);
            return [$commentId];
        } else {
            $this->getResponse()->setCode(400);
            return ['Field is empty'];
        }
    }

};
