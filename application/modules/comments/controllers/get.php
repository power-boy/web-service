<?php
/**
 * Get comments by photo id
 *
 * @author   Taras Seryogin
 * @created  20.07.11 18:39
 */
namespace Application;

use Bluz;
use Application\Auth;
use Application\Comments;
use Swagger\Annotations as SWG;


return

    /**
     * @package
     * @category
     *
     * @SWG\Resource(
     *   apiVersion="0.0.1",
     *   basePath="/",
     *   resourcePath="/comments",
     *   @SWG\Produces("application/json")
     * )
     * @SWG\Api(path="/browse",
     *   @SWG\Operation(
     *     method="GET",
     *     summary="Get comments by photo id",
     *     notes="",
     *     type="User",
     *     nickname="comment",
     *     @SWG\Parameter(
     *       name="photoId",
     *       description="id photo",
     *       required=true,
     *       type="string",
     *       paramType="query"
     *     ),
     *     @SWG\ResponseMessage(code=404, message="Comment not found"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden")
     *   )
     * )
     */

    /**
     * @method GET
     * @route /browse
     * @param int $photoId
     * @return \closure
     */

function ($photoId) {

    /**
     *  @var \Application\Bootstrap $this
     */

    $this->useJson();
    $userId = $this->getAuth()->getIdentity()->id;
    if (empty($userId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        if (!empty($photoId)) {
            $comments = Comments\Table::getInstance()->getComments($photoId);
            if (!empty($comments)) {
                return $comments;
            } else {
                $this->getResponse()->setCode(404);
                return ['Not found coments for this photo'];
            }
        } else {
            $this->getResponse()->setCode(400);
            return ['Field is empty'];
        }

    }
};
