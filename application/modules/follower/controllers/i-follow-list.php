<?php
/**
 * List  of people whom I follow
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/follower",
     *      @SWG\Api(path="/follower/i-follow-list",
     *          @SWG\Operation(
     *              method="GET",
     *              summary="The list of people whom I follow",
     *              notes="",
     *              type="string",
     *              nickname="get-user-list",
     *
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden")
     *   )
     * )
     * )
     */
    /**
     * @method GET
     * @return \closure
     */
function () {

    $this->useJson();
    $followerId = $this->getAuth()->getIdentity()->id;
    if (empty($followerId)) {
        $this->getResponse()->setCode(403);
        $this->getResponse()->setHeader('Forbidden', 403);
        return ['You are not signed'];
    } else {
        return  Users\Table::getInstance()->iFollowList($followerId);
    }
};
