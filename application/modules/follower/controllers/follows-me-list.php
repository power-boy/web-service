<?php
/**
 * List of people who follow me
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/follower",
     *      @SWG\Api(path="/follower/follows-me-list",
     *          @SWG\Operation(
     *              method="GET",
     *              summary="A list of people who follow me",
     *              notes="",
     *              type="string",
     *              nickname="follow-the-user",
     *
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden")
     *   )
     * )
     * )
     */
    /**
     * @method GET
     * @return \closure
     */
function () {

    $this->useJson();
    $userId = $this->getAuth()->getIdentity()->id;
    if (empty($userId)) {
        $this->getResponse()->setCode(403);
        $this->getResponse()->setHeader('Forbidden', 403);
        return ['You are not signed'];
    } else {
        return  Users\Table::getInstance()->followMeList($userId);
    }
};
