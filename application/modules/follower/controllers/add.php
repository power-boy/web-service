<?php
/**
 * Follow someone
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/follower",
     *      @SWG\Api(path="/following",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="follow someone",
     *              notes="",
     *              type="string",
     *              nickname="follow someone",
     *                @SWG\Parameter(
     *                 name="userId",
     *                 description="User Id  per which you want to follow",
     *                 required=true,
     *                 type="string",
     *                 paramType="form"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden"),
     *     @SWG\ResponseMessage(code=404, message="Not found")
     *   )
     * )
     * )
     */
    /**
     * @method POST
     * @route /following
     * @return \closure
     */
function ($userId) {

    $this->useJson();
    $followerId = $this->getAuth()->getIdentity()->id;
    if (empty($followerId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        if (!empty($userId)) {
            if ($userId == $followerId) {
                    return ['You can not sign up for yourself, subscribe to another user'];
            } else {
                    $checked = Followers\Table::getInstance()->checkUser($userId);
                if (empty($checked)) {
                    $this->getResponse()->setCode(404);
                    return ['User not found'];
                } else {
                        $id =  Followers\Table::getInstance()->addFollower($followerId, $userId);
                        $this->getResponse()->setCode(201);
                        return ['You have subscribed to this user'];
                }
            }
        } else {
            $this->getResponse()->setCode(400);
            return ['Field is empty'];
        }
    }
};
