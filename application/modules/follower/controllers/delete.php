<?php
/**
 * Unsubscribe from user
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Bluz;
use Swagger\Annotations as SWG;
use Bluz\Controller;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/follower",
     *      @SWG\Api(path="/follower/delete",
     *          @SWG\Operation(
     *              method="DELETE",
     *              summary="unsubscribe",
     *              notes="",
     *              type="string",
     *              nickname="unsubscribe",
     *                @SWG\Parameter(
     *                 name="userId",
     *                 description=" user ID, ​​from which you want to unsubscribe",
     *                 required=true,
     *                 type="string",
     *                 paramType="form"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden")
     *   )
     * )
     * )
     */
    /**
     * @method DELETE
     * @return \closure
     */
function () {

    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $followerId = $this->getAuth()->getIdentity()->id;
    if (empty($followerId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        $restController = new Controller\Rest();
        $userId = $restController->getData('userId');
        if (!empty($userId)) {
            if ($userId == $followerId) {
                $this->getResponse()->setCode(400);
                return ['you can not unsubscribe from himself'];
            } else {
                $checked = Followers\Table::getInstance()->checkUser($userId);
                if (empty($checked)) {
                    $this->getResponse()->setCode(404);
                    return ['User not found'];
                } else {
                    Followers\Table::getInstance()->deleteFollower($followerId, $userId);
                    $this->getResponse()->setCode(204);
                    return ['You have unsubscribed from the user'];
                }
            }
        } else {
            $this->getResponse()->setCode(400);
            return ['Field is empty'];
        }
    }
};
