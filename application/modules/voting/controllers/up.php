<?php
/**
 * Voting up
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return

    /**
     * @package
     * @category
     *
     * @SWG\Resource(
     *   apiVersion="0.0.1",
     *   basePath="/",
     *   resourcePath="/voting",
     *   @SWG\Produces("application/json")
     * )
     * @SWG\Api(path="/voting/up",
     *   @SWG\Operation(
     *     method="GET",
     *     summary="Voting up",
     *     notes="",
     *     type="User",
     *     nickname="Voting",
     *     @SWG\Parameter(
     *       name="photoId",
     *       description="Id photo for the voting",
     *       required=true,
     *       type="string",
     *       paramType="query"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden"),
     *     @SWG\ResponseMessage(code=404, message="Not found")
     *   )
     * )
     */

    /**
     * @method GET
     * @return \closure
     */
function ($photoId) {
    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $userId = $this->getAuth()->getIdentity()->id;
    if (empty($userId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        if (!empty($photoId)) {
            $check = Media\Table::getInstance()->checkExistPhoto($photoId);
            if (!empty($check)) {
                $vote = Votes\Table::getInstance()->checkExistVote($userId, $photoId);
                if (!empty($vote)) {
                    Votes\Table::getInstance()->updateVote(1, $userId, $photoId);
                } else {
                    Votes\Table::getInstance()->insertVote(1, $userId, $photoId);
                }
                $this->getResponse()->setCode(201);
                return ['You have voted'];
            } else {
                $this->getResponse()->setCode(404);
                return ['This photo does not exist'];
            }
        } else {
            $this->getResponse()->setCode(400);
            return ['Field is empty'];
        }
    }
};
