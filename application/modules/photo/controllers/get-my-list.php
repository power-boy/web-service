<?php
/**
 * Get User Photo in JSON
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/photo",
     *      @SWG\Api(path="/photo/get-my-list",
     *          @SWG\Operation(
     *              method="GET",
     *              summary="get User Photo",
     *              notes="",
     *              type="string",
     *              nickname="photo",
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden"),
     *     @SWG\ResponseMessage(code=404, message="Not found")
     *   )
     * )
     * )
     */

    /**
     * @method GET
     * @route /photo/get-my-list
     * @return \closure
     */
function () {

    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $uri = $this->getRouter()->getFullUrl();
    $user = $this->getAuth()->getIdentity()->id;
    if (empty($user)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        $result = Media\Table::getInstance()->getMyList($user, $uri);
        if (!empty($result)) {
            return $result;
        } else {
            $this->getResponse()->setCode(404);
            return ['Photo not found'];
        }
    }
};
