<?php
/**
 * Get my friends list images in JSON
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/photo",
     *      @SWG\Api(path="/photo/get-my-friends-list",
     *          @SWG\Operation(
     *              method="GET",
     *              summary="get my friends list",
     *              notes="",
     *              type="string",
     *              nickname="photos of friends",
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden"),
     *     @SWG\ResponseMessage(code=404, message="Not found")
     *   )
     * )
     * )
     */

    /**
     * @method GET
     * @route /photo/get-my-friends-list
     * @return \closure
     */
function () {
    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $uri = $this->getRouter()->getFullUrl();
    $followerId = $this->getAuth()->getIdentity()->id;
    if (empty($followerId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        $result = Followers\Table::getInstance()->myFriendsList($followerId, $uri);
        if (!empty($result)) {
            return $result;
        } else {
            $this->getResponse()->setCode(404);
            return ['Photo not found'];
        }
    }
};
