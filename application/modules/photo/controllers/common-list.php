<?php
/**
 * List of user images in JSON
 *
 * @category Application
 *
 * @author   Taras Seryogin
 * @created  12.02.13 14:18
 */
namespace Application;

use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/photo",
     *      @SWG\Api(path="/photo/common-list",
     *          @SWG\Operation(
     *              method="GET",
     *               summary="get list from all",
     *               notes="",
     *                type="string",
     *                nickname="photos from all",
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden"),
     *     @SWG\ResponseMessage(code=404, message="Not found")
     *   )
     * )
     * )
     */

    /**
     * @method GET
     * @return \closure
     */
function () {
    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $uri = $this->getRouter()->getFullUrl();
    $userId = $this->getAuth()->getIdentity()->id;
    if (empty($userId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        $result = Followers\Table::getInstance()->getCommonList($uri);
        if (!empty($result)) {
            return $result;
        } else {
            $this->getResponse()->setCode(404);
            return ['Photo not found'];
        }
    }
};
