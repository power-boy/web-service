<?php
/**
 * CRUD for media
 */
namespace Application;

use Application\Media;
use Bluz\Controller;
use Swagger\Annotations as SWG;

return
    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/photo",
     *      @SWG\Api(path="/photos",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="Upload Photo",
     *              notes="",
     *              type="string",
     *              nickname="uploadPhoto",
     *              @SWG\Parameter(
     *                   name="title",
     *                   description="title photo",
     *                   required=false,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *              @SWG\Parameter(
     *                   name="file",
     *                   description="uploded photo",
     *                   required=true,
     *                   type="file",
     *                   paramType="body"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request"),
     *     @SWG\ResponseMessage(code=403, message="Forbidden")
     *   )
     * )
     * )
     */

    /**
     * @method POST
     * @route /photos
     * @return \closure
     */
function () {

    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $this->getSession()->getStore()->start();
    $userId = $this->getAuth()->getIdentity()->id;
    if (empty($userId)) {
        $this->getResponse()->setCode(403);
        return ['You are not signed'];
    } else {
        $crud = Media\Crud::getInstance();
        $crud->setUploadDir('uploads/'.$userId.'/media');

        $crudController = new Controller\Crud();
        $crudController->setCrud($crud);

        $result = $crudController();
        if (isset($result['errors'])) {
            $this->getResponse()->setCode(400);
            if (isset($result['errors']['file'])) {
                foreach ($result['errors']['file'] as $val) {
                    if ($val == Media\Crud::NO_FILE_CHOSEN) {
                        return [Media\Crud::NO_FILE_CHOSEN];
                    }
                }
            }
        } else {
            return ['File successfully loaded'];
        }

    }

};
