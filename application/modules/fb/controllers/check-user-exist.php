<?php
/**
 * User registration
 *
 * @category Application
 *
 * @author   Anton Shevchuk
 * @created  09.11.12 13:19
 */
namespace Application;

use Application\Users;
use Bluz\Controller;

return

    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/fb",
     *      @SWG\Api(path="/fb/check-user-exist",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="registration",
     *              notes="",
     *              type="string",
     *              nickname="userRegister",
     *              @SWG\Parameter(
     *                   name="email",
     *                   description="User email",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *            @SWG\Parameter(
     *                   name="login",
     *                   description="User Login",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),

     *     @SWG\ResponseMessage(code=400, message="Bad Request")
     *   )
     * )
     * )
     */

    /**
     * @method POST
     * @return \closure
     */
function () {
    /**
     * @var \Application\Bootstrap $this
     */
    $this->useJson();
    $crudController = new Controller\Crud();
    $crudController->setCrud(CheckUsersExist\Crud::getInstance());
    return $crudController();
};
