<?php
/**
 * Facebook login
 *
 * @category Application
 *
 * @author   Anton Shevchuk
 * @created  09.11.12 13:19
 */
namespace Application;

use Application\UsersFb;
use Application\Auth\Table;
use Application\Facebook;

return

    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/fb",
     *      @SWG\Api(path="/fb/signin",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="login",
     *              notes="",
     *              type="string",
     *              nickname="userRegister",
     *              @SWG\Parameter(
     *                   name="accessToken",
     *                   description="facebook accessToken",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Bad Request")
     *   )
     * )
     * )
     */

    /**
     * @method POST
     * @return \closure
     */
    function () {
        /**
         * @var \Application\Bootstrap $this
         */
    $this->useJson();
    $options = $this->getConfigData('auth', 'facebook');
    if (!$options || !isset($options['appId']) || empty($options['secret'])) {
        throw new Exception('Facebook authorization is not configured');
    }

    $facebook = new Facebook\Facebook(
        array(
            'appId'  => $options['appId'],
            'secret' => $options['secret'],
        )
    );

    $facebook->destroySession();
    $accessToken = $this->getRequest()->getPost('accessToken');
    if (!empty($accessToken)) {
        $facebook->setAccessToken($accessToken);
        $fbId = $facebook->getUser();
        $row = Auth\Table::getInstance()->getAuthRow(Table::PROVIDER_FACEBOOK, $fbId);
        if ($row) {
            // if user has been registered
            $user = Users\Table::findRow($row->userId);
            return $user;
        }
    } else {
        $this->getResponse()->setCode(400);
        $this->getResponse()->setHeader('Bad Request', 400);
        return ['Field is empty'];
    }

};

