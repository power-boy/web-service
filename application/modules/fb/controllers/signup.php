<?php
/**
 * User registration
 *
 * @category Application
 *
 * @author   Anton Shevchuk
 * @created  09.11.12 13:19
 */
namespace Application;

use Application\Users;
use Bluz\Controller;
use Application\Facebook;
use Signup\FbSignupController;

return

    /**
     * @SWG\Resource(
     *      basePath="/",
     *      resourcePath="/fb",
     *      @SWG\Api(path="/fb/signup",
     *          @SWG\Operation(
     *              method="POST",
     *              summary="registration",
     *              notes="",
     *              type="string",
     *              nickname="userRegister",
     *              @SWG\Parameter(
     *                   name="accessToken",
     *                   description="facebook accessToken",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *              @SWG\Parameter(
     *                   name="email",
     *                   description="User email",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *             @SWG\Parameter(
     *                   name="name",
     *                   description="User name",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *            @SWG\Parameter(
     *                   name="login",
     *                   description="User Login",
     *                   required=true,
     *                   type="string",
     *                   paramType="form"
     *     ),
     *           @SWG\Parameter(
     *                   name="password",
     *                   description="User password",
     *                   type="string",
     *                   paramType="form"
     *     ),
     *          @SWG\Parameter(
     *                   name="password2",
     *                   description="Repeat password",
     *                   type="string",
     *                   paramType="form"
     *     ),

     *     @SWG\ResponseMessage(code=400, message="Bad Request")
     *   )
     * )
     * )
     */

    /**
     * @method POST
     * @return \closure
     */
function () {
    /**
     * @var \Application\Bootstrap $this
    */

    $this->useJson();
    $options = $this->getConfigData('auth', 'facebook');
    if (!$options || !isset($options['appId']) || empty($options['secret'])) {
        throw new Exception('Facebook authorization is not configured');
    }

    $facebook = new Facebook\Facebook(
        array(
            'appId'  => $options['appId'],
            'secret' => $options['secret'],
        )
    );

    $facebook->destroySession();

    $crudController = new FbSignupController();
    $accessToken = $crudController->getData('accessToken');
    $facebook->setAccessToken($accessToken);
    $fbId = $facebook->getUser();
    $crudController->changeData(array('foreignKey' => $fbId));
    $crudController->setCrud(UsersFbApi\Crud::getInstance());
    return $crudController();
};
