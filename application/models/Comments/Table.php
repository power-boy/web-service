<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Comments;

/**
 * Pages Table
 *
 * @category Application
 * @package  Pages
 */
class Table extends \Bluz\Db\Table
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'comments';

    public function getComments($photoId)
    {
        $comments = $this->fetch('SELECT c.photoId, c.userId, c.comment FROM comments AS c WHERE  photoId = ? ', [ $photoId ]);
        $result = array();
        foreach ($comments as $comment) {
            $result[] = [
                "photoId" => $comment->photoId,
                "userId" => $comment->userId,
                "comment" => $comment->comment,
            ];
        }
        return $result;

    }

    public function addComments($userId, $photoId, $comment)
    {
        return  $this->insert(['userId' => $userId, 'photoId' => $photoId, 'comment' => $comment]);

    }
}
