<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\CheckUsersExist;

use Application\Auth;
use Application\Exception;
use Application\Auth\Table;
use Application\Users;
use Application\UsersRoles;
use Application\UsersActions;
use Bluz\Crud\ValidationException;
use Application\Users\Table as Table1;

/**
 * Crud
 *
 * @category Application
 * @package  Users
 *
 * @author   Anton Shevchuk
 * @created  30.10.12 16:11
 */
class Crud extends \Bluz\Crud\Table
{
    /**
     * @param $data
     * @throws \Application\Exception
     * @return boolean
     */
    public function createOne($data)
    {
        $this->validate(null, $data);
        $this->validateCreate($data);
        $this->checkErrors();
    }

    /**
     * @throws ValidationException
     */
    public function validateCreate($data)
    {
        // login
        $this->checkLogin($data);

        $login = isset($data['login'])?$data['login']:null;
        // check unique
        if ($this->getTable()->findRowWhere(['login' => $login])) {
            $this->addError(
                __('User with login "%s"  exists', esc($login)),
                'login'
            );
        }

        // email
        $this->checkEmail($data);

        $email = isset($data['email'])?$data['email']:null;
        // TODO: add solution for check gmail accounts (because a.s.d@gmail.com === asd@gmail.com)
        // check unique
        if ($this->getTable()->findRowWhere(['email' => $email])) {
            $this->addError(
                __('User with email "%s"  exists', esc($email)),
                'email'
            );
        }

    }

    /**
     * @throws ValidationException
     */
    public function validateUpdate($id, $data)
    {
        // name validator
        $this->checkLogin($data);

        // email validator
        $this->checkEmail($data);
    }

    /**
     * checkLogin
     *
     * @param $data
     * @return void
     */
    protected function checkLogin($data)
    {
        $login = isset($data['login'])?$data['login']:null;
        if (empty($login)) {
            $this->addError('Login can\'t be empty', 'login');
        }
        if (strlen($login) > 255) {
            $this->addError('Login can\'t be bigger than 255 symbols', 'login');
        }
    }

    /**
     * checkEmail
     *
     * @param array $data
     * @return boolean
     */
    public function checkEmail($data)
    {
        $email = isset($data['email'])?$data['email']:null;

        if (empty($email)) {
            $this->addError('Email can\'t be empty', 'email');
            return false;
        }

        if (strlen($email) > 255) {
            $this->addError('Email can\'t be bigger than 255 symbols', 'email');
            return false;
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            list($user, $domain) = explode("@", $email, 2);
            if (!checkdnsrr($domain, "MX") && !checkdnsrr($domain, "A")) {
                $this->addError('Email has invalid domain name', 'email');
                return false;
            }
        } else {
            $this->addError('Email is invalid', 'email');
            return false;
        }
        return true;
    }
}