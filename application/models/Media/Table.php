<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Media;

/**
 * Table
 *
 * @category Application
 * @package  Media
 */
class Table extends \Bluz\Db\Table
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'media';

    /**
     * Primary key(s)
     * @var array
     */
    protected $primary = array('id');

    public function getMyList($user, $uri)
    {
        $images = $this->fetch("SELECT
                                        m.id AS photoId,
                                        m.title,
                                        m.file,
                                        m.preview,
                                        m.created,
                                        u.id AS userId,
                                        u.login,
                                        u.`name`,
                                        SUM(v.vote) AS sum_v
                                    FROM
                                        media AS m
                                    JOIN users AS u ON m.userId = u.id
                                    LEFT JOIN votes AS v ON m.id = v.photoId
                                    WHERE
                                        type LIKE 'image/%' AND m.userId = ?
                                    GROUP BY
                                        m.id", [$user]);

        $result = array();
        foreach ($images as $image) {
            $result[] = [
                "photoId" => $image->photoId,
                "title" => $image->title,
                "image" => $uri.$image->file,
                "thumb" => $uri.$image->preview,
                "created" => $image->created,
                "userId" => $image->userId,
                "login" => $image->login,
                "name" => $image->name,
                "mark" => $image->sum_v
            ];
        }
        return $result;
    }

    public function checkExistPhoto($photoId)
    {
        $check = $this->fetch('SELECT id FROM media WHERE  `id` = ? ', [ $photoId ]);
        return $check;
    }
}
