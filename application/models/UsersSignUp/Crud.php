<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\UsersSignUp;

use Application\Auth;
use Application\Exception;

use Application\UsersActions;
use Bluz\Crud\ValidationException;

/**
 * Crud
 *
 * @category Application
 * @package  Users
 *
 * @author   Anton Shevchuk
 * @created  30.10.12 16:11
 */
class Crud extends \Bluz\Crud\Table
{
    const LOGIN_EMPTY = 'Login can\'t be empty';
    const LOGIN_IS_BIGGER = 'Login can\'t be bigger than 255 symbols';
    const NAME_IS_BIGGER ='Name can\'t be bigger than 255 symbols';
    const EMAIL_IS_BIGGER = 'Email can\'t be bigger than 255 symbols';
    const EMAIL_HAS_INVALID_DOMAINE = 'Email has invalid domain name';
    const EMAIL_IS_INVALID = 'Email is invalid';
    const PASSWORD_EMPTY = 'Password can\'t be empty';
    const URI_EMPTY = 'Uri can\'t be empty';
    const NAME_EMPTY = 'Name can\'t be empty';
    const EMAIL_EMPTY = 'Email can\'t be empty';
    const PASSWORD_NOT_EQUALS = 'Password is not equal';
    const USER_ALREADY_EXIST = 'User with login "%s" already exists';
    const EMAIL_ALREADY_EXIST = 'User with email "%s" already exists';

    /**
     * @param $data
     * @throws \Application\Exception
     * @return boolean
     */
    public function createOne($data)
    {
        $this->validate(null, $data);
        $this->validateCreate($data);
        $this->checkErrors();

        /** @var $row Row */
        $row = $this->getTable()->create();
        $row->setFromArray($data);
        $row->status = Table::STATUS_PENDING;
        $row->save();

        $userId = $row->id;

        // create auth
        $password = isset($data['password'])?$data['password']:null;
        Auth\Table::getInstance()->generateEquals($row, $password);

        // create activation token
        // valid for 5 days
        $actionRow = UsersActions\Table::getInstance()->generate($userId, UsersActions\Table::ACTION_ACTIVATION, 5);

        // send activation email
        // generate activation URL

        $activationUrl = $data['uri'].'/code/'.$actionRow->code.'/id/'.$userId;

        $subject = "Activation";

        $body = app()->dispatch(
            'users',
            'mail-template',
            [
                'template' => 'registration',
                'vars' => ['user' => $row, 'activationUrl' => $activationUrl, 'password' => $password]
            ]
        )->render();

        try {
            $mail = app()->getMailer()->create();

            // subject
            $mail->Subject = $subject;
            $mail->MsgHTML(nl2br($body));

            $mail->AddAddress($data['email']);

            app()->getMailer()->send($mail);

        } catch (\Exception $e) {
            app()->getLogger()->log(
                'error',
                $e->getMessage(),
                ['module' => 'users', 'controller' => 'change-email', 'userId' => $userId]
            );

            throw new Exception('Unable to send email. Please contact administrator.');
        }

        return $userId;
    }

    /**
     * @throws ValidationException
     */
    public function validateCreate($data)
    {
        // login
        $this->checkLogin($data);
        //name
        $this->checkName($data);

        if (empty($data['uri'])){
            $this->addError(self::URI_EMPTY, 'uri');
        }

        $login = isset($data['login'])?$data['login']:null;
        // check unique
        if ($this->getTable()->findRowWhere(['login' => $login])) {
            $this->addError(
                __(self::USER_ALREADY_EXIST, esc($login)),
                'login'
            );
        }

        // email
        $this->checkEmail($data);

        $email = isset($data['email'])?$data['email']:null;
        // TODO: add solution for check gmail accounts (because a.s.d@gmail.com === asd@gmail.com)
        // check unique
        if ($this->getTable()->findRowWhere(['email' => $email])) {
            $this->addError(
                __(self::EMAIL_ALREADY_EXIST, esc($email)),
                'email'
            );
        }

        // password
        $password = isset($data['password'])?$data['password']:null;
        $password2 = isset($data['password2'])?$data['password2']:null;
        if (empty($password)) {
            $this->addError(self::PASSWORD_EMPTY, 'password');
        }

        if ($password !== $password2) {
            $this->addError(self::PASSWORD_NOT_EQUALS, 'password2');
        }
    }

    /**
     * @throws ValidationException
     */
    public function validateUpdate($id, $data)
    {
        // name validator
        $this->checkLogin($data);

        // email validator
        $this->checkEmail($data);
    }

    /**
     * checkLogin
     *
     * @param $data
     * @return void
     */
    protected function checkLogin($data)
    {
        $login = isset($data['login'])?$data['login']:null;
        if (empty($login)) {
            $this->addError(self::LOGIN_EMPTY, 'login');
        }
        if (strlen($login) > 255) {
            $this->addError(self::LOGIN_IS_BIGGER, 'login');
        }
    }

    /**
     * checkName
     *
     * @param $data
     * @return void
     */
    protected function checkName($data)
    {
        $name = isset($data['name'])?$data['name']:null;
        if (empty($name)) {
            $this->addError(self::NAME_EMPTY, 'name');
        }
        if (strlen($name) > 255) {
            $this->addError(self::NAME_IS_BIGGER, 'name');
        }
    }

    /**
     * checkEmail
     *
     * @param array $data
     * @return boolean
     */
    public function checkEmail($data)
    {
        $email = isset($data['email'])?$data['email']:null;

        if (empty($email)) {
            $this->addError(self::EMAIL_EMPTY, 'email');
            return false;
        }

        if (strlen($email) > 255) {
            $this->addError(self::EMAIL_IS_BIGGER, 'email');
            return false;
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            list($user, $domain) = explode("@", $email, 2);
            if (!checkdnsrr($domain, "MX") && !checkdnsrr($domain, "A")) {
                $this->addError(self::EMAIL_HAS_INVALID_DOMAINE, 'email');
                return false;
            }
        } else {
            $this->addError(self::EMAIL_IS_INVALID, 'email');
            return false;
        }
        return true;
    }
}