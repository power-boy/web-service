<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Followers;

/**
 * Pages Table
 *
 * @category Application
 * @package  Pages
 */
class Table extends \Bluz\Db\Table
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'followers';

    public function checkUser($userId)
    {
        $checked = $this->fetch('SELECT login FROM `users` WHERE `id` = ?', [$userId]);
        return $checked;
    }

    public function addFollower($followerId, $userId)
    {
        $id = $this->insert(['followerId' => $followerId, 'userId' => $userId]);
        return $id;
    }

    public function deleteFollower($followerId, $userId)
    {
        $id = $this->delete(['followerId' => $followerId, 'userId' => $userId]);
        return $id;
    }

    public function getCommonList($uri)
    {
        $images = $this->fetch("SELECT
                                    m.id AS photoId,
                                    m.title,
                                    m.file,
                                    m.preview,
                                    m.created,
                                    u.id AS userId,
                                    u.login,
                                    u.`name`,
                                    SUM(v.vote) AS sum_v
                                FROM
                                    media AS m
                                JOIN users AS u ON m.userId = u.id
                                LEFT JOIN votes AS v ON m.id = v.photoId
                                WHERE
                                    type LIKE 'image/%'
                                GROUP BY
                                    m.id");
        $result = array();
        foreach ($images as $image) {
            $result[] = [
                "photoId" => $image->photoId,
                "title" => $image->title,
                "image" => $uri.$image->file,
                "thumb" => $uri.$image->preview,
                "created" => $image->created,
                "userId" => $image->userId,
                "login" => $image->login,
                "name" => $image->name,
                "mark" => $image->sum_v
            ];
        }
        return $result;
    }

    public function myFriendsList($followerId, $uri)
    {
        $usersId = $this->fetch('SELECT
                                    f.userId
                                  FROM
                                    followers AS f
                                  WHERE
                                    f.followerId =?', [$followerId]);
        foreach ($usersId as $userId) {
            $images[] = $this->fetch("SELECT
                                        m.id AS photoId,
                                        m.title,
                                        m.file,
                                        m.preview,
                                        m.created,
                                        u.id AS userId,
                                        u.login,
                                        u.`name`,
                                        SUM(v.vote) AS sum_v
                                    FROM
                                        media AS m
                                    JOIN users AS u ON m.userId = u.id
                                    LEFT JOIN votes AS v ON m.id = v.photoId
                                    WHERE
                                        type LIKE 'image/%' AND m.userId = ?
                                    GROUP BY
                                        m.id", [$userId->userId]);
        }
        $result = array();
        foreach ($images as $image) {
            foreach ($image as $val) {
                $result[] = [
                    "photoId" => $val->photoId,
                    "title" => $val->title,
                    "image" => $uri.$val->file,
                    "thumb" => $uri.$val->preview,
                    "created" => $val->created,
                    "userId" => $val->userId,
                    "login" => $val->login,
                    "name" => $val->name,
                    "mark" => $val->sum_v
                ];
            }
        }
        return $result;
    }
}
