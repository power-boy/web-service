<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Users;

/**
 * Table
 *
 * @category Application
 * @package  Users
 *
 * @author   Anton Shevchuk
 * @created  08.07.11 17:36
 */
class Table extends \Bluz\Db\Table
{
    /**
     * Pending email verification
     */
    const STATUS_PENDING = 'pending';
    /**
     * Active user
     */
    const STATUS_ACTIVE = 'active';
    /**
     * Disabled by administrator
     */
    const STATUS_DISABLED = 'disabled';
    /**
     * Removed account
     */
    const STATUS_DELETED = 'deleted';
    /**
     * system user with ID=0
     */
    const SYSTEM_USER = 0;

    /**
     * Table
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Primary key(s)
     * @var array
     */
    protected $primary = array('id');

    public function followMeList($userId)
    {
        $list = $this->fetch('SELECT
                                u.login,
                                u.`name`
                              FROM
                                users AS u
                              JOIN followers AS f ON f.followerId = u.id
                              WHERE
                                f.userId = ?', [$userId]);
        $result = array();
        foreach ($list as $person) {
            $result[] = [
                "login" => $person->login,
                "name" => $person->name,
            ];
        }
        return $result;
    }

    public function iFollowList($followerId)
    {
        $list = $this->fetch('SELECT
                                    u.login,
                                    u.`name`
                                FROM
                                    users AS u
                                JOIN followers AS f ON f.userId = u.id
                                WHERE
                                    f.followerId = ?', [$followerId]);
        $result = array();
        foreach ($list as $person) {
            $result[] = [
                "login" => $person->login,
                "name" => $person->name,
            ];
        }
        return $result;
    }

    public function search($login)
    {
        $list = $this->fetch('SELECT
                                    u.login,
                                    u.name
                                FROM
                                    users AS u
                                WHERE
                                    u.login LIKE (?)
                                OR u.`name` LIKE (?)', ['%'.$login.'%', '%'.$login.'%']);
        $result = array();
        foreach ($list as $person) {
            $result[] = [
                "login" => $person->login,
                "name" => $person->name,
            ];
        }
        return $result;
    }
}
