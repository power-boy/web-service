<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Votes;

/**
 * Pages Table
 *
 * @category Application
 * @package  Pages
 */
class Table extends \Bluz\Db\Table
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'votes';

    public function checkExistVote($userId, $photoId)
    {
        $vote = $this->fetch('SELECT vote FROM votes WHERE `userId` = ? AND `photoId` = ?', [$userId, $photoId ]);
        return $vote;
    }

    public function updateVote($vote, $userId, $photoId)
    {
        $this->update(['vote' => $vote], ['userId' => $userId, 'photoId' => $photoId]);
    }

    public function insertVote($vote, $userId, $photoId)
    {
        $this->insert(['vote' => $vote, 'userId' => $userId, 'photoId' => $photoId]);
    }
}
