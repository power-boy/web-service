<?php
/**
 * Taras Seryogin
 */
namespace Signup;

 use Bluz\Controller\Crud;

class FbSignupController extends Crud
{
    public function __construct()
    {
        parent::__construct();

    }

    public function changeData (array $newData)
    {
        $this->data['foreignKey'] = $newData['foreignKey'];
    }
}
